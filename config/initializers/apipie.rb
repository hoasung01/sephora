Apipie.configure do |config|
  config.app_name                = "Sephora"
  config.copyright               = "&copy; #{Date.current.year} nguyenngochai.shipagent@gmail.com"
  config.api_base_url            = "/api"
  config.doc_base_url            = "/apipie"
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
  config.app_info                = "Sephora API documentation"
  config.api_routes              = Rails.application.routes
end