class Product < ApplicationRecord
  scope :category, -> (category) {where("lower(category) like ?", "#{(category).downcase}%")}
  scope :price, -> (price) {where price: price}
  scope :sort_price, -> (sort_type) {order(price: "#{(sort_type).downcase}".to_sym)}
end