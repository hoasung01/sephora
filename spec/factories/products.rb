FactoryGirl.define do
  factory :product do
    name FFaker::Product.product_name
    sold_out false
    category FFaker::Product.model
    under_sale false
    price Random.rand(1200..1500)
    sale_price Random.rand(900..1100)
  end
end