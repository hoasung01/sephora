puts "create products"
Product.delete_all

products = []
20.times do |i|
  product = Product.create!({name: FFaker::Product.product_name, category: FFaker::Product.model, price: Random.rand(1200..1500), sale_price: Random.rand(900..1100)})
  products.push(product)
end