module Api::V1
  class ProductsController < ApiController
    include Document::Products
    PER_PAGE = 10
    before_action :set_product, only: :show

    def index
      @products = Product.where(nil)
      filtering_params(params).each do |key, value|
        @products = @products.public_send(key, value) if value.present?
      end
      paginate json: @products, per_page: PER_PAGE
    end

    def show
      render json: @product
    end

    private
      def set_product
        @product = Product.find(params[:id])
      end

      def product_params
        params.require(:product).permit(:name, :sold_out, :category, :under_sale, :price, :sale_price)
      end

      def filtering_params(params)
        params.slice(:category, :price, :sort_price)
      end
  end
end