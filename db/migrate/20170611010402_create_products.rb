class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.boolean :sold_out
      t.string :category
      t.boolean :under_sale
      t.integer :price, default: 0
      t.integer :sale_price, default: 0

      t.timestamps
    end
  end
end
