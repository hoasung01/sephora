module Api::V1
  module Document::Products
    extend Apipie::DSL::Concern

    api :GET, "/v1/products", "listing products"
    param :category, String, desc: "filter products based on category (insensitive case)", required: false
    param :price, Fixnum, desc: "filter products based on price", required: false
    param :sort_price, String, desc: "sort descending or ascening products based on price (insensitive case)", required: false
    param :page, Fixnum, desc: "paging", required: false
    description "listing products with filtering and sorting based on category, price"
    formats ['json']
    example " 'products': [{
              'id': 22, 
              'name': 'GPS Auto Transmitter', 
              'sold_out': false, 
              'category': 'M-4355', 
              'under_sale': false, 
              'price': 1477, 
              'sale_price': 921
             },
             {
              'id': 23,
              'name': 'Tag Controller',
              'sold_out': false,
              'category': 'EZ-7935',
              'under_sale': false,
              'price': 1211,
              'sale_price': 929
            }] "
    def index  
    end

    api :GET, "/v1/products/:id", "show a product"
    error code: 404, desc: "not found a product", meta: {message: I18n.t('errors.messages.not_found'), status: :not_found}
    param :id, Fixnum, desc: "product ID", required: true
    description "show a product with specific id"
    example " 'product': { 
              'id': 22, 
              'name': 'GPS Auto Transmitter', 
              'sold_out': false, 
              'category': 'M-4355', 
              'under_sale': false, 
              'price': 1477, 
              'sale_price': 921
             } "
    formats ['json']
    def show
    end
  end
end