class AddDefaultToSoldOutAndUnderSaleToProducts < ActiveRecord::Migration[5.1]
  def change
    change_column :products, :sold_out, :boolean, null: false, default: false
    change_column :products, :under_sale, :boolean, null: false, default: false
  end
end