Rails.application.routes.draw do
  apipie
  namespace :api do 
    namespace :v1 do
      resources :products, only: [:index, :show]
    end
  end
end