require 'rails_helper'

RSpec.describe Api::V1::ProductsController, type: :controller do
  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all products as @products" do
      product = create(:product)
      get :index, params: {}, session: valid_session
      expect(assigns(:products)).to eq([product])
    end

    context 'filter with category' do
      it "lower case" do
        product_1 = create(:product, category: 'M-4355')
        product_2 = create(:product, category: 'EZ-7935')
        product_3 = create(:product, category: 'SI-6350')
        get :index, params: {category: 'm'}, session: valid_session
        expect(assigns(:products)).to eq([product_1])
        expect(assigns(:products)).not_to eq([product_2, product_3])
      end

      it "upper case" do
        product_1 = create(:product, category: 'M-4355')
        product_2 = create(:product, category: 'EZ-7935')
        product_3 = create(:product, category: 'SI-6350')
        get :index, params: {category: 'M'}, session: valid_session
        expect(assigns(:products)).to eq([product_1])
        expect(assigns(:products)).not_to eq([product_2, product_3])
      end
    end

    it 'filter with price' do
      product_1 = create(:product, price: 1000)
      product_2 = create(:product, price: 1500)
      get :index, params: {price: '1000'}, session: valid_session
      expect(assigns(:products)).to eq([product_1])
      expect(assigns(:products)).not_to eq([product_2])
    end

    context 'sorting with price' do
      it "ascending" do
        product_1 = create(:product, price: 1000)
        product_2 = create(:product, price: 2500)
        product_3 = create(:product, price: 1500)
        get :index, params: {sort_price: 'asc'}, session: valid_session
        expect(assigns(:products)).to eq([product_1, product_3, product_2])
      end

      it "descending" do
        product_1 = create(:product, price: 1000)
        product_2 = create(:product, price: 2500)
        product_3 = create(:product, price: 1500)
        get :index, params: {sort_price: 'desc'}, session: valid_session
        expect(assigns(:products)).to eq([product_2, product_3, product_1])
      end
    end
  end

  describe "GET #show" do
    it "assigns the requested product as @product" do
      product = create(:product)
      get :show, params: {id: product.to_param}, session: valid_session
      expect(assigns(:product)).to eq(product)
    end
  end
end