require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'scope' do
    it 'with category' do
      product_1 = create(:product, category: 'M-4355')
      product_2 = create(:product, category: 'EZ-7935')
      product_3 = create(:product, category: 'SI-6350')
      expect(Product.category('m')).to include product_1
      expect(Product.category('m')).not_to include([product_2, product_3])
      expect(Product.category('M')).to include product_1
      expect(Product.category('M')).not_to include([product_2, product_3])
      expect(Product.category('EZ')).to include product_2
      expect(Product.category('EZ')).not_to include([product_1, product_3])
    end

    it 'with price' do
      product_1 = create(:product, price: 1000)
      product_2 = create(:product, price: 1500)
      expect(Product.price('1000')).to include product_1
      expect(Product.price('1000')).not_to include product_2
    end

    it 'with sort_price' do
      product_1 = create(:product, price: 1000)
      product_2 = create(:product, price: 2500)
      product_3 = create(:product, price: 1500)
      expect(Product.sort_price('asc')).to eq [product_1, product_3, product_2]
      expect(Product.sort_price('ASC')).to eq [product_1, product_3, product_2]
      expect(Product.sort_price('aSC')).to eq [product_1, product_3, product_2]
      expect(Product.sort_price('desc')).to eq [product_2, product_3, product_1]
      expect(Product.sort_price('DESC')).to eq [product_2, product_3, product_1]
      expect(Product.sort_price('dEsC')).to eq [product_2, product_3, product_1]
    end
  end
end